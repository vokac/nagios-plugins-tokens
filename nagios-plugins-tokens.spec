%define dir /usr/lib64/nagios/plugins
%if 0%{?rhel} == 7
  %define dist .el7
%else
  %define dist .el9
%endif

Summary: Nagios plugins for managing Scientifc Tokens (Scitokens)
Name: nagios-plugins-tokens
Version: 0.1.5
Release: 1%{?dist}
License: ASL 2.0
Group: Network/Monitoring
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch
%if 0%{?el7}
Requires: python-nap
Requires: pexpect
Requires: oidc-agent-cli
%else
Requires: python3-nap
Requires: pexpect
Requires: oidc-agent-cli
%endif
%if 0%{?el7}
%else
BuildRequires: python3-setuptools python3-devel /usr/bin/pathfix.py
%endif

%description

%prep
%setup -q
%if 0%{?el7}
%else
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" refresh_token
%endif

%build

%install
rm -rf $RPM_BUILD_ROOT
install --directory ${RPM_BUILD_ROOT}%{dir}
install --mode 755 ./refresh_token ${RPM_BUILD_ROOT}%{dir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{dir}/refresh_token
