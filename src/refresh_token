#!/usr/bin/env python
#
# refresh_token - nagios plugin to manage tokens
#
# Author: Marian Babik
# Copyright CERN 2021
#
from __future__ import print_function
import os
import logging
import pexpect
import base64
import json
import nap.core

log = logging.getLogger()


def run(cmd, cmd_timeout=300, log_file=None):
    log.debug("    Subprocess %s starting" % cmd)
    try:
        cmd_out, ret_code = pexpect.run(' '.join(cmd), timeout=cmd_timeout, withexitstatus=True, env=os.environ,
                                        logfile=log_file)
    except pexpect.ExceptionPexpect as e:
        log.exception("    Subprocess %s failed with (%s)" % (cmd, e))
        return False, None, e

    log.debug("    Subprocess {} ({}) finished".format(cmd, ret_code))
    log.debug("    Subprocess output: %s " % cmd_out)

    return True, ret_code, cmd_out


app = nap.core.Plugin()
app.add_argument("--token-config", help="Token configuration file name (usually under .oidc-agent)")
app.add_argument("--aud", help="Enable token audience generation in path")
app.add_argument("--token-time", type=int, help="Token lifetime to request")
app.add_argument("-x", "--token-out", help="Token output file")
app.add_argument("-E", "--oidc-env", help="OIDC environment file", default='/omd/sites/etf/.oidc-agent/oidc-env.sh')


@app.metric()
def test_refresh(args, io):
    if args.oidc_env and not os.path.isfile(args.oidc_env):
        io.status = nap.CRITICAL
        io.summary = 'OIDC-AGENT environment not found ({})'.format(args.oidc_env)
        return
    with open(args.oidc_env) as of:
        oidc_env = of.readlines()
        for var in oidc_env:
            if '=' in var and ';' in var:
                vi = var.strip().split(';')[0]
                os.environ[vi.split('=')[0]] = vi.split('=')[1]
    log.debug(os.environ)
    if 'OIDC_SOCK' not in os.environ.keys():
        io.status = nap.CRITICAL
        io.summary = 'OIDC-AGENT not running or env. not configured'
        return

    if not args.token_config:
        io.status = nap.CRITICAL
        io.summary = 'OIDC agent token configuration file not specified'
        return

    if args.aud and not os.path.exists(args.aud):
        io.status = nap.CRITICAL
        io.summary = 'Token generation path not found {}'.format(args.aud)
        return

    token_lifetime = ''
    if args.token_time:
        token_lifetime = '--time {}'.format(args.token_time)

    if args.token_out:
        st, ret_code, cmd_out = run(['/bin/oidc-token', token_lifetime, args.token_config])
        if not st or ret_code != 0:
            io.status = nap.CRITICAL
            io.summary = 'Failed to run oidc-token, check agent'
            print('Exception was caught or unexpected return code: {}'.format(cmd_out))
            return
        with os.fdopen(os.open(args.token_out, os.O_WRONLY | os.O_CREAT, 0o600), 'wb') as tf:
            tf.write(cmd_out)

        with open(args.token_out, 'rb') as tf:
            token = tf.read()
        token = str(token.decode('utf-8'))
        if '.' in token:
            token = token.split(".")[1]
            padding = 4 - (len(token) % 4)
            token = json.loads(base64.b64decode(token + padding * "="))
            print(' WLCG scitoken info:')
            for k, v in token.items():
                print('  {} : {}'.format(k, v))

    elif args.aud:
        hosts = os.listdir(args.aud)
        for host in hosts:
            aud = '{}:9619'.format(host)
            scopes = '-s compute.read -s compute.cancel -s compute.create -s compute.modify'
            t_out = os.path.join(args.aud, host)
            if not os.path.isdir(t_out):
                continue
            st, ret_code, cmd_out = run(['/bin/oidc-token', '--aud \"{}\"'.format(aud), token_lifetime, scopes,
                                         args.token_config])
            if not st or ret_code != 0:
                io.status = nap.CRITICAL
                io.summary = 'Failed to run oidc-token, check agent'
                print('Exception was caught or unexpected return code: {}'.format(cmd_out))
                return
            with open(os.path.join(t_out, 'token'), 'w') as tf_out:
                print(t_out)
                tf_out.write(cmd_out)

    io.status = nap.OK
    io.summary = "OIDC token(s) refresh successful"


if __name__ == '__main__':
    app.run()
